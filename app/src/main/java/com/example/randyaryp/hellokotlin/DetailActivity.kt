package com.example.randyaryp.hellokotlin

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import org.jetbrains.anko.*

class DetailActivity : AppCompatActivity() {

    private var name: String = ""
    private var detail: String = ""
    lateinit var nameTextView: TextView
    lateinit var detailTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        verticalLayout(){
            padding = dip(16)
            nameTextView = textView().lparams(width = matchParent){
                topMargin = dip(5)
            }
            detailTextView = textView().lparams(width = matchParent){
                topMargin = dip(5)
            }
        }

        val intent = intent
        name = intent.getStringExtra("name")
        nameTextView.text = name
        detail = intent.getStringExtra("detail")
        detailTextView.text = detail
    }
}