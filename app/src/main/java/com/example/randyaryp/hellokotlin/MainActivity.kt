package com.example.randyaryp.hellokotlin

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.TextView
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

class MainActivity : AppCompatActivity() {

    private var items: MutableList<Item> = mutableListOf()

    private fun initData(){
        val name = resources.getStringArray(R.array.club_name)
        val image = resources.obtainTypedArray(R.array.club_image)
        val detail = resources.getStringArray(R.array.club_image)
        items.clear()
        for (i in name.indices) {
            items.add(Item(name[i],
                    image.getResourceId(i, 0), detail[i]))
        }

        //Recycle the typed array
        image.recycle()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val list = findViewById<RecyclerView>(R.id.club_list)
        initData()

        list.layoutManager = LinearLayoutManager(this)
        list.adapter = RecyclerViewAdapter(this, items)
    }

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        MainActivityUI().setContentView(this)
//    }
//
//    class MainActivityUI : AnkoComponent<MainActivity> {
//        override fun createView(ui: AnkoContext<MainActivity>) = with(ui) {
//            verticalLayout{
//                padding = dip(16)
//                val name = editText{
//                    hint = "What's your name?"
//                }
//                button("Say Hello"){
//                    backgroundColor = ContextCompat.getColor(context, colorAccent)
//                    textColor = Color.WHITE
//                    onClick { toast("Hello, ${name.text}!") }
//                }.lparams(width = matchParent){
//                    topMargin = dip(5)
//                }
//                button("Show Selector"){
//                    backgroundColor = ContextCompat.getColor(context, colorAccent)
//                    textColor = Color.WHITE
//
//                    onClick {
//                        val club = listOf("Barcelona", "Real Madrid", "Bayern Munchen", "Liverpool")
//                        selector("Hello, ${name.text}! What's football club do you love?", club) { _, i ->
//                            toast("So you love ${club[i]}, right?")
//                        }
//                    }
//                }.lparams(width = matchParent){
//                    topMargin = dip(5)
//                }
//                button("Show Snackbar"){
//                    backgroundColor = ContextCompat.getColor(context, colorAccent)
//                    textColor = Color.WHITE
//                    onClick {
//                        snackbar(name, "Hello, ${name.text}!")
//                    }
//                }.lparams(width = matchParent){
//                    topMargin = dip(5)
//                }
//                button("Show Progress Bar"){
//                    backgroundColor = ContextCompat.getColor(context, colorAccent)
//                    textColor = Color.WHITE
//                    onClick {
//                        indeterminateProgressDialog("Hello, ${name.text}! Please wait...").show()
//                    }
//                }.lparams(width = matchParent){
//                    topMargin = dip(5)
//                }
//                button("Go to Second Activity"){
//                    backgroundColor = ContextCompat.getColor(context, colorAccent)
//                    textColor = Color.WHITE
//                    onClick {
//                        startActivity<DetailActivity>("name" to "${name.text}")
//                    }
//                }.lparams(width = matchParent){
//                    topMargin = dip(5)
//                }
//
//            }
//
//        }
//    }
}
